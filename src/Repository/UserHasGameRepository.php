<?php

namespace App\Repository;

use App\Entity\UserHasGame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserHasGame|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserHasGame|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserHasGame[]    findAll()
 * @method UserHasGame[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserHasGameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserHasGame::class);
    }

     /**
      * @return UserHasGame[] Returns an array of UserHasGame objects
     */

    public function findByUserTitleAsc($user)
    {
        return $this->createQueryBuilder('u')
            ->addSelect('g')
            ->addSelect('p')
            ->innerJoin('u.game', 'g', 'WITH')
            ->innerJoin('u.platform', 'p', 'WITH')
            ->andWhere('u.user = :val')
            ->setParameter('val', $user)
            ->orderBy('g.title_game', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByUserAll($user, $type, $platform, $order)
    {
        $query = $this->createQueryBuilder('u')
                ->addSelect('g')
                ->addSelect('p')
                ->innerJoin('u.game', 'g', 'WITH')
                ->innerJoin('u.platform', 'p', 'WITH')
                ->innerJoin('g.type', 't', 'WITH')
                ->andWhere('u.user = :val')
                ->setParameter('val', $user)
                ;

        if($type !== 'all') {
            $query = $query -> andWhere('t.title_type = :type')
                            -> setParameter('type', $type);
        }

        if($platform !== 'all'){
           $query = $query -> andWhere('p.name_plat = :platform')
                           -> setParameter('platform', $platform)
           ;
        }

        $query -> orderBy('g.title_game', $order)
            ;

        return $query ->getQuery()
                      ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?UserHasGame
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
