<?php

namespace App\Controller;

use App\Entity\UserHasGame;
use App\Form\UserHasGameType;
use App\Repository\PlatformRepository;
use App\Repository\TypeRepository;
use App\Repository\UserHasGameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/mygames")
 */
class UserHasGameController extends AbstractController
{
    /**
     * @Route("/", name="user_has_game_index", methods={"GET"})
     */
    public function index(Request $request, UserHasGameRepository $userHasGameRepository, PlatformRepository $platformRepository, TypeRepository $typeRepository): Response
    {
        $user = $this->getUser();

$test = $request->query->keys();
dump($test);
$test1 = $request->query->get('platform');
dump($test1);

        if ($request->query->keys() !== []) {

            if (($request->query->get('platform') !== null) || ($request->query->get('platform') !== 'all')) {
                $platform = $request->query->get('platform');
            } else {
                $platform = 'all';
            }

            if (($request->get('type') !== null) || ($request->get('type') !== 'all')) {
                $type = $request->get('type');
            } else {
                $type = 'all';
            }

            if ($request->get('order') !== null) {
                $order = $request->get('order');
            } else {
                $order = 'ASC';
            }

            $oui = $userHasGameRepository->findByUserAll($user, $type, $platform, $order);
        } else {
            $oui = $userHasGameRepository->findByUserTitleAsc($user);
        }

        dump($request);
        dump($oui);

        $platforms = $platformRepository->findAll();
        $types = $typeRepository->findAll();

        return $this->render('user_has_game/index.html.twig', [
            'user_has_games' => $oui,
            'platforms' => $platforms,
            'types' => $types,
        ]);
    }

    /**
     * @Route("/new", name="user_has_game_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $userHasGame = new UserHasGame();
        $user = $this->getUser();
        $form = $this->createForm(UserHasGameType::class, $userHasGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userHasGame->setUser($user);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userHasGame);
            $entityManager->flush();

            return $this->redirectToRoute('user_has_game_index');
        }

        return $this->render('user_has_game/new.html.twig', [
            'user_has_game' => $userHasGame,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_has_game_show", methods={"GET"})
     */
    public function show(UserHasGame $userHasGame): Response
    {
        return $this->render('user_has_game/show.html.twig', [
            'user_has_game' => $userHasGame,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_has_game_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, UserHasGame $userHasGame): Response
    {
        $form = $this->createForm(UserHasGameType::class, $userHasGame);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_has_game_index');
        }

        return $this->render('user_has_game/edit.html.twig', [
            'user_has_game' => $userHasGame,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_has_game_delete", methods={"POST"})
     */
    public function delete(Request $request, UserHasGame $userHasGame): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userHasGame->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userHasGame);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_has_game_index');
    }
}
