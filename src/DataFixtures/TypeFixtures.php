<?php

namespace App\DataFixtures;

use App\Entity\Type;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TypeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($count = 0; $count < 20; $count++) {
            $type = new Type();
            $type->setTitleType("Titre " . $count);
            $type->setDescriptionType("Description Fixture" . $count);
            $manager->persist($type);
        }

        $manager->flush();
    }
}