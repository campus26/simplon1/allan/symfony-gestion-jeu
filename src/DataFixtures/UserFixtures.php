<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder){
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {

        $user = new User();

        $user->setEmail("bonjour@gmail.com");
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'oui'
        ));
        $user->setRoles(['ROLE_ADMIN']);

        $user1 = new User();

        $user1->setEmail("test@gmail.com");
        $user1->setPassword($this->passwordEncoder->encodePassword(
            $user1,
            'test'
        ));
        $user1->setRoles(['ROLE_ADMIN']);

        $manager->persist($user);
        $manager->persist($user1);

        $manager->flush();
    }
}
