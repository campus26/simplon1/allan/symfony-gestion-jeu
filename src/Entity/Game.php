<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GameRepository::class)
 */
class Game
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title_game;

    /**
     * @ORM\Column(type="text")
     */
    private $description_game;

    /**
     * @ORM\Column(type="date")
     */
    private $released_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $game_cover;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nb_sell_game;

    /**
     * @ORM\ManyToMany(targetEntity=Type::class, inversedBy="game")
     */
    private $type;

    /**
     * @ORM\ManyToMany(targetEntity=Platform::class, inversedBy="game")
     */
    private $platform;

    /**
     * @ORM\OneToMany(targetEntity=UserHasGame::class, mappedBy="game", orphanRemoval=true)
     */
    private $user_has_game;

    public function __construct()
    {
        $this->type = new ArrayCollection();
        $this->platform = new ArrayCollection();
        $this->user_has_name = new ArrayCollection();
        $this->user_has_game = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleGame(): ?string
    {
        return $this->title_game;
    }

    public function setTitleGame(string $title_game): self
    {
        $this->title_game = $title_game;

        return $this;
    }

    public function getDescriptionGame(): ?string
    {
        return $this->description_game;
    }

    public function setDescriptionGame(string $description_game): self
    {
        $this->description_game = $description_game;

        return $this;
    }

    public function getReleasedAt(): ?\DateTimeInterface
    {
        return $this->released_at;
    }

    public function setReleasedAt(\DateTimeInterface $released_at): self
    {
        $this->released_at = $released_at;

        return $this;
    }

    public function getGameCover(): ?string
    {
        return $this->game_cover;
    }

    public function setGameCover(?string $game_cover): self
    {
        $this->game_cover = $game_cover;

        return $this;
    }

    public function getNbSellGame(): ?int
    {
        return $this->nb_sell_game;
    }

    public function setNbSellGame(?int $nb_sell_game): self
    {
        $this->nb_sell_game = $nb_sell_game;

        return $this;
    }

    /**
     * @return Collection|type[]
     */
    public function getType(): Collection
    {
        return $this->type;
    }

    public function addType(type $type): self
    {
        if (!$this->type->contains($type)) {
            $this->type[] = $type;
        }

        return $this;
    }

    public function removeType(type $type): self
    {
        $this->type->removeElement($type);

        return $this;
    }

    /**
     * @return Collection|platform[]
     */
    public function getPlatform(): Collection
    {
        return $this->platform;
    }

    public function addPlatform(platform $platform): self
    {
        if (!$this->platform->contains($platform)) {
            $this->platform[] = $platform;
        }

        return $this;
    }

    public function removePlatform(platform $platform): self
    {
        $this->platform->removeElement($platform);

        return $this;
    }

    /**
     * @return Collection|userhasgame[]
     */
    public function getUserHasName(): Collection
    {
        return $this->user_has_name;
    }

    public function addUserHasName(userhasgame $userHasName): self
    {
        if (!$this->user_has_name->contains($userHasName)) {
            $this->user_has_name[] = $userHasName;
            $userHasName->setGame($this);
        }

        return $this;
    }

    public function removeUserHasName(userhasgame $userHasName): self
    {
        if ($this->user_has_name->removeElement($userHasName)) {
            // set the owning side to null (unless already changed)
            if ($userHasName->getGame() === $this) {
                $userHasName->setGame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|userhasgame[]
     */
    public function getUserHasGame(): Collection
    {
        return $this->user_has_game;
    }

    public function addUserHasGame(userhasgame $userHasGame): self
    {
        if (!$this->user_has_game->contains($userHasGame)) {
            $this->user_has_game[] = $userHasGame;
            $userHasGame->setGame($this);
        }

        return $this;
    }

    public function removeUserHasGame(userhasgame $userHasGame): self
    {
        if ($this->user_has_game->removeElement($userHasGame)) {
            // set the owning side to null (unless already changed)
            if ($userHasGame->getGame() === $this) {
                $userHasGame->setGame(null);
            }
        }

        return $this;
    }
}
