<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $title_type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description_type;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, mappedBy="type")
     */
    private $game;

    public function __construct()
    {
        $this->game = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitleType(): ?string
    {
        return $this->title_type;
    }

    public function setTitleType(string $title_type): self
    {
        $this->title_type = $title_type;

        return $this;
    }

    public function getDescriptionType(): ?string
    {
        return $this->description_type;
    }

    public function setDescriptionType(?string $description_type): self
    {
        $this->description_type = $description_type;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getGame(): Collection
    {
        return $this->game;
    }

    public function addGame(Game $game): self
    {
        if (!$this->game->contains($game)) {
            $this->game[] = $game;
            $game->addType($this);
        }

        return $this;
    }

    public function removeGame(Game $game): self
    {
        if ($this->game->removeElement($game)) {
            $game->removeType($this);
        }

        return $this;
    }
}
