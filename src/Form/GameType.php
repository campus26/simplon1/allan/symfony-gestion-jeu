<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Platform;
use App\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title_game')
            ->add('description_game')
            ->add('released_at', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('game_cover',FileType::class, [
                    'mapped' => false,
                    'required' => false,
                ])
            ->add('nb_sell_game')
            ->add('type', EntityType::class,[
                'class' => Type::class,
                'choice_label' => 'title_type',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('platform', EntityType::class,[
                'class' => Platform::class,
                'choice_label' => 'name_plat',
                'multiple' => true,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Game::class,
        ]);
    }
}
