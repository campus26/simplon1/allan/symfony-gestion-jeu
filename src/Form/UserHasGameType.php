<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Platform;
use App\Entity\UserHasGame;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserHasGameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owned_at', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('platform', EntityType::class,[
                'class' => Platform::class,
                'choice_label' => 'name_plat',
                'multiple' => true,
                'expanded' => true,
            ])
            ->add('game', EntityType::class,[
                'class' => Game::class,
                'choice_label' => 'title_game',
                'multiple' => false,
                'expanded' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserHasGame::class,
        ]);
    }
}
