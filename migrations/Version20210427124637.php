<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210427124637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_has_game DROP FOREIGN KEY FK_9EFA0ED3A76ED395');
        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE platform_user_has_game DROP FOREIGN KEY FK_89468F957CE205E4');
        $this->addSql('DROP INDEX IDX_89468F957CE205E4 ON platform_user_has_game');
        $this->addSql('ALTER TABLE platform_user_has_game DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE platform_user_has_game DROP FOREIGN KEY FK_89468F95FFE6496F');
        $this->addSql('ALTER TABLE platform_user_has_game CHANGE userhasgame_id user_has_game_id INT NOT NULL');
        $this->addSql('ALTER TABLE platform_user_has_game ADD CONSTRAINT FK_96DFCE6F2A65784B FOREIGN KEY (user_has_game_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_96DFCE6F2A65784B ON platform_user_has_game (user_has_game_id)');
        $this->addSql('ALTER TABLE platform_user_has_game ADD PRIMARY KEY (platform_id, user_has_game_id)');
        $this->addSql('DROP INDEX idx_89468f95ffe6496f ON platform_user_has_game');
        $this->addSql('CREATE INDEX IDX_96DFCE6FFFE6496F ON platform_user_has_game (platform_id)');
        $this->addSql('ALTER TABLE platform_user_has_game ADD CONSTRAINT FK_89468F95FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX IDX_9EFA0ED3A76ED395 ON user_has_game');
        $this->addSql('ALTER TABLE user_has_game DROP user_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email_user VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, password_user VARCHAR(100) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, role LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE platform_user_has_game DROP FOREIGN KEY FK_96DFCE6F2A65784B');
        $this->addSql('DROP INDEX IDX_96DFCE6F2A65784B ON platform_user_has_game');
        $this->addSql('ALTER TABLE platform_user_has_game DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE platform_user_has_game DROP FOREIGN KEY FK_96DFCE6FFFE6496F');
        $this->addSql('ALTER TABLE platform_user_has_game CHANGE user_has_game_id userhasgame_id INT NOT NULL');
        $this->addSql('ALTER TABLE platform_user_has_game ADD CONSTRAINT FK_89468F957CE205E4 FOREIGN KEY (userhasgame_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_89468F957CE205E4 ON platform_user_has_game (userhasgame_id)');
        $this->addSql('ALTER TABLE platform_user_has_game ADD PRIMARY KEY (platform_id, userhasgame_id)');
        $this->addSql('DROP INDEX idx_96dfce6fffe6496f ON platform_user_has_game');
        $this->addSql('CREATE INDEX IDX_89468F95FFE6496F ON platform_user_has_game (platform_id)');
        $this->addSql('ALTER TABLE platform_user_has_game ADD CONSTRAINT FK_96DFCE6FFFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_has_game ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_has_game ADD CONSTRAINT FK_9EFA0ED3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_9EFA0ED3A76ED395 ON user_has_game (user_id)');
    }
}
