<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210414102632 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE platform_userhasgame (platform_id INT NOT NULL, userhasgame_id INT NOT NULL, INDEX IDX_89468F95FFE6496F (platform_id), INDEX IDX_89468F957CE205E4 (userhasgame_id), PRIMARY KEY(platform_id, userhasgame_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE platform_userhasgame ADD CONSTRAINT FK_89468F95FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE platform_userhasgame ADD CONSTRAINT FK_89468F957CE205E4 FOREIGN KEY (userhasgame_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE platform_userhasgame');
    }
}
