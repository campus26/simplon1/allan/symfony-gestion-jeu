<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210504072717 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE user_has_game_platform (user_has_game_id INT NOT NULL, platform_id INT NOT NULL, INDEX IDX_C8F0E8C12A65784B (user_has_game_id), INDEX IDX_C8F0E8C1FFE6496F (platform_id), PRIMARY KEY(user_has_game_id, platform_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user_has_game_platform ADD CONSTRAINT FK_C8F0E8C12A65784B FOREIGN KEY (user_has_game_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_has_game_platform ADD CONSTRAINT FK_C8F0E8C1FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE platform_user_has_game');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE platform_user_has_game (platform_id INT NOT NULL, user_has_game_id INT NOT NULL, INDEX IDX_96DFCE6F2A65784B (user_has_game_id), INDEX IDX_96DFCE6FFFE6496F (platform_id), PRIMARY KEY(platform_id, user_has_game_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE platform_user_has_game ADD CONSTRAINT FK_89468F95FFE6496F FOREIGN KEY (platform_id) REFERENCES platform (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE platform_user_has_game ADD CONSTRAINT FK_96DFCE6F2A65784B FOREIGN KEY (user_has_game_id) REFERENCES user_has_game (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE user_has_game_platform');
    }
}
